# Imports
from binance.client import Client

import numpy as np
import pandas as pd

import plotly.graph_objects as go

API_kEY = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
SECRET_KEY = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"

ASSET = "BTCUSDT"
INTERVAL = Client.KLINE_INTERVAL_1HOUR
START_TIME = "1 week ago UTC"

# Fetch Data
print("[*] Fetch Data")
# Binance API Client
client = Client(API_kEY, SECRET_KEY)
# Get historical klines
fetched_data = client.get_historical_klines(ASSET, INTERVAL, START_TIME)
# Keep only the first 6 columns
data = np.array(fetched_data)[:, 0:6]
# Create a DataFrame with annotated columns
df = pd.DataFrame(data, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
# Convert Date from ms to Datetime
df["Date"] = pd.to_datetime(df["Date"], unit="ms")
# Convert columns to numeric values
df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
df["High"] = pd.to_numeric(df["High"], errors="coerce")
df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
print("[*] Generate chart")
# Create Chart
chart = go.Figure(layout=go.Layout(title=(ASSET + " " + INTERVAL + " over " + START_TIME + " Chart"), template="plotly_dark"))
# Candlesticks
chart.add_trace(go.Candlestick(x=df["Date"],
                               open=df["Open"],
                               high=df["High"],
                               low=df["Low"],
                               close=df["Close"],
                               increasing_line_color='#2ebd85', decreasing_line_color='#e0294a',
                               increasing_fillcolor='#2ebd85', decreasing_fillcolor='#e0294a',
                               name="Price"))
# Average Close Price
chart.add_trace(go.Scatter(x=df["Date"],
                           y=df["Close"],
                           line_color="#80deea",
                           name="Close"))
# Display Chart
chart.show()
