import ccxt
print('CCXT version:', ccxt.__version__)  # requires CCXT version > 1.20.31
exchange = ccxt.binance({
    'apiKey': 'YOUR_API_KEY',
    'secret': 'YOUR_API_SECRET',
    'enableRateLimit': True,
    'options': {
        'defaultType': 'future',  # ←-------------- quotes and 'future'
    },
})

exchange.load_markets()

# exchange.verbose = True  # uncomment this line if it doesn't work

# your code here...