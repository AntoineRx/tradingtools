"""
Scalping Strategy
"""

# Imports
from datetime import datetime, timedelta

from binance.client import Client
import numpy as np
import pandas as pd
from docutils.nodes import date

from ta import add_all_ta_features
from ta.utils import dropna
from ta.volatility import BollingerBands
from ta.trend import IchimokuIndicator
from ta.momentum import RSIIndicator

import plotly.graph_objects as go
from plotly.subplots import make_subplots

import dash
import dash_core_components as dcc
import dash_html_components as html

import mplfinance as mpf

# Data
print("[*] Data")
api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"

# Init
print("[*] Init")
client = Client(api_key, secret_key)

print("[*] Get Market Data")
# fetch 1 Hour klines for the last week
btc_usdt_1m_4h = client.get_historical_klines("BTCUSDT", Client.KLINE_INTERVAL_1MINUTE, "4 hour ago UTC")
# Keep only the first 6 columns
btc_usdt_1m_4h = np.array(btc_usdt_1m_4h)[:, 0:6]
# Create a Tableau with annotated columns
df = pd.DataFrame(btc_usdt_1m_4h, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
# Convert Date from ms to Datetime
df["Date"] = pd.to_datetime(df["Date"], unit="ms")
# df.set_index("Date", inplace=True)
# Convert columns to numeric values
df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
df["High"] = pd.to_numeric(df["High"], errors="coerce")
df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
#print(df)


def extendframe(df, ndays):
    """
    (df, ndays) -> df that is padded by ndays in beginning and end
    """
    df3 = pd.DataFrame({'Date': df["Date"].tail(ndays) + pd.DateOffset(minutes=ndays)})
    return df.append(df3, ignore_index=True, sort=True)



# Initialize Bollinger Bands Indicator
indicator_bb = BollingerBands(close=df["Close"], n=20, ndev=2)
# Add Bollinger Bands features
df['bb_bbm'] = indicator_bb.bollinger_mavg()
df['bb_bbh'] = indicator_bb.bollinger_hband()
df['bb_bbl'] = indicator_bb.bollinger_lband()
# Add Bollinger Band high indicator
df['bb_high'] = indicator_bb.bollinger_hband_indicator()
# Add Bollinger Band low indicator
df['bb_low'] = indicator_bb.bollinger_lband_indicator()
# Add Width Size Bollinger Bands
df['bb_width'] = indicator_bb.bollinger_wband()
# Add Percentage Bollinger Bands
df['bb_perc'] = indicator_bb.bollinger_pband()

indicator_ichimoku = IchimokuIndicator(high=df["High"], low=df["Low"], n1=9, n2=26, n3=52)


# Add Senkou Span A
df['ich_senkou_a'] = indicator_ichimoku.ichimoku_a()
# Add Senkou Span B
df['ich_senkou_b'] = indicator_ichimoku.ichimoku_b()
#
df_extra = pd.DataFrame({'Date': df["Date"].tail(26) + pd.DateOffset(minutes=26)})
df = df.append(df_extra, ignore_index=True, sort=True)
df['ich_senkou_a'] = df['ich_senkou_a'].shift(26)
df['ich_senkou_b'] = df['ich_senkou_b'].shift(26)
# Red Cloud
df['kumo_green'] = df['ich_senkou_b'].mask(df['ich_senkou_b'] > df['ich_senkou_a'], df['ich_senkou_a'])
df['kumo_red'] = df['ich_senkou_a'].mask(df['ich_senkou_a'] > df['ich_senkou_b'], df['ich_senkou_b'])


# Add Kijun-sen
df['ich_kijun'] = indicator_ichimoku.ichimoku_base_line()
# Add Tenkan-sen
df['ich_tenkan'] = indicator_ichimoku.ichimoku_conversion_line()
# Add Lagging Span
df['ich_lagging'] = df["Close"].shift(-26)

indicator_rsi = RSIIndicator(close=df["Close"], n=14)
df['rsi'] = indicator_rsi.rsi()


# Create Chart
fig = go.Figure(layout=go.Layout(title="BTC/USDT 1Hour over 1Week Chart", template="plotly_dark"))

# Candlesticks
fig.add_trace(go.Candlestick(x=df["Date"],
                             open=df["Open"],
                             high=df["High"],
                             low=df["Low"],
                             close=df["Close"],
                             increasing_line_color='#2ebd85', decreasing_line_color='#e0294a',
                             increasing_fillcolor='#2ebd85', decreasing_fillcolor='#e0294a',
                             name="Price",
                             yaxis="y2"))

blue="rgba(69, 203, 227, 0.42)"
# Bollinger High
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["bb_bbh"],
                         line_color=blue,
                         name="BB High",
                         fill=None))

# Bollinger Low
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["bb_bbl"],
                         line_color=blue,
                         name="BB Low",
                         fill="tonexty",
                         fillcolor=blue))

red = "rgba(206, 75, 77, 0.62)"
green = "rgba(46, 168, 68, 0.62)"
# Senkou Span A
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["ich_senkou_a"],
                         line_color=green,
                         name="Senkou Span A",
                         fill=None,
                         connectgaps=True))


fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["kumo_green"],
                         line_color=red,
                         name="kumo green",
                         fill="tonexty",
                         fillcolor=green,
                         connectgaps=False))

# Senkou Span B
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["ich_senkou_b"],
                         line_color=red,
                         name="Senkou Span B",
                         fill=None,
                         connectgaps=True))

fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["kumo_red"],
                         line_color=green,
                         name="kumo red",
                         fill="tonexty",
                         fillcolor=red,
                         connectgaps=False))

# Tenkan
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["ich_tenkan"],
                         line_color="cyan",
                         name="Tenkan",
                         fill=None,
                         connectgaps=True))

purple="rgba(230, 86, 225, 1)"
# Kijun
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["ich_kijun"],
                         line_color=purple,
                         name="Kijun",
                         fill=None,
                         connectgaps=True))

# Lagging
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["ich_lagging"],
                         line_color="rgba(44, 237, 73, 1)",
                         name="Lagging",
                         fill=None))


padding = 0.005
range = [df["Low"].min() * (1 - padding), df["High"].max() * (1 + padding)]

fig.update_layout(xaxis_rangeslider_visible=False, yaxis=dict(range=range),
                  yaxis2=dict(overlaying='y', range=range))
fig.show()
