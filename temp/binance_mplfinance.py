# Imports
from binance.client import Client
import mplfinance as mpf
import numpy as np
import pandas as pd

"""
Get data from Binance & Display chart.
"""
# Data
print("[*] Data")
api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"

# Init
print("[*] Init")
client = Client(api_key, secret_key)

print("[*] Get Market Data")
# fetch 1 minute klines for the last day up until now
btc_usdt_5m_1d = client.get_historical_klines("BTCUSDT", Client.KLINE_INTERVAL_5MINUTE, "1 day ago UTC")
"""
Returned format:
[
  [
    1499040000000,      // Open time (in miliseconds)
    "0.01634790",       // Open
    "0.80000000",       // High
    "0.01575800",       // Low
    "0.01577100",       // Close
    "148976.11427815",  // Volume
    1499644799999,      // Close time
    "2434.19055334",    // Quote asset volume
    308,                // Number of trades
    "1756.87402397",    // Taker buy base asset volume
    "28.46694368",      // Taker buy quote asset volume
    "17928899.62484339" // Ignore.
  ]
]
"""
# Keep only the first 6 columns
chart_btc_usdt_5m_1d = np.array(btc_usdt_5m_1d)[:, 0:6]
# Create a Tableau with annotated columns
df = pd.DataFrame(chart_btc_usdt_5m_1d, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
# Convert Date from ms to Datetime
df["Date"] = pd.to_datetime(df["Date"], unit="ms")
# Set Date column as index
df.set_index("Date", inplace=True)
df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
df["High"] = pd.to_numeric(df["High"], errors="coerce")
df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
print(df)
# Create Chart Style
mc = mpf.make_marketcolors(
                            up='#2ebd85', down='#e0294a',
                            edge={'up': '#2ebd85', 'down': '#e0294a'},
                            wick={'up': '#2ebd85', 'down': '#e0294a'},
                            inherit=True
                           )

s = mpf.make_mpf_style(base_mpf_style='mike', figcolor="#111111", facecolor="#111111", gridstyle="solid", marketcolors=mc)
"""
# Multiple data to plot
apds = [ mpf.make_addplot(tcdf),
         mpf.make_addplot(low_signal,scatter=True,markersize=200,marker='^'),
         mpf.make_addplot(high_signal,scatter=True,markersize=200,marker='v'),
       ]
       
# fill area (kumo): https://matplotlib.org/gallery/lines_bars_and_markers/fill_between_demo.html#sphx-glr-gallery-lines-bars-and-markers-fill-between-demo-py

mpf.plot(df,addplot=apds,figscale=1.25,volume=True)
"""
# Display chart
mpf.plot(df, type="candle", volume=True, style=s, title="BTC/USDT 5min over 1day")
# Save Chart
#mpf.plot(df, type="candle", volume=True, style=s, title="BTC/USDT 5min over 1day", savefig="chart_btc_usdt_5m_1d.png")
