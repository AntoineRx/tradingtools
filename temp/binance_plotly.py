# Imports
from binance.client import Client
import numpy as np
import pandas as pd

import plotly.graph_objects as go

import dash
import dash_core_components as dcc
import dash_html_components as html

"""
Get data from Binance & Display chart.
"""
# Data
print("[*] Data")
api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"

# Init
print("[*] Init")
client = Client(api_key, secret_key)

print("[*] Get Market Data")
# fetch 1 Hour klines for the last week
btc_usdt_1h_7d = client.get_historical_klines("BTCUSDT", Client.KLINE_INTERVAL_1HOUR, "1 week ago UTC")
# Keep only the first 6 columns
chart_btc_usdt_1h_7d = np.array(btc_usdt_1h_7d)[:, 0:6]
# Create a Tableau with annotated columns
df = pd.DataFrame(chart_btc_usdt_1h_7d, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
# Convert Date from ms to Datetime
df["Date"] = pd.to_datetime(df["Date"], unit="ms")
# Convert columns to numeric values
df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
df["High"] = pd.to_numeric(df["High"], errors="coerce")
df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
print(df)
# Create Chart
fig = go.Figure(layout=go.Layout(title="BTC/USDT 1Hour over 1Week Chart", template="plotly_dark"))
# Candlesticks
fig.add_trace(go.Candlestick(x=df["Date"],
                             open=df["Open"],
                             high=df["High"],
                             low=df["Low"],
                             close=df["Close"],
                             increasing_line_color= '#2ebd85', decreasing_line_color= '#e0294a',
                             increasing_fillcolor= '#2ebd85', decreasing_fillcolor= '#e0294a',
                             name="Price"))

# Average Close Price
fig.add_trace(go.Scatter(x=df["Date"],
                         y=df["Close"],
                         line_color="cyan",
                         name="Close"))

fig.update_layout(xaxis_rangeslider_visible=False)
fig.show()