# Imports
from binance.client import Client
from binance.websockets import BinanceSocketManager
from binance.enums import *

from twisted.internet import reactor

import numpy as np
import pandas as pd

import plotly.graph_objects as go

import dash
import dash_core_components as dcc
import dash_html_components as html
import signal
import time

"""
Get data from Binance & Display chart.
"""


class BinanceManager:
    def __init__(self, api_key, secret_key):
        # Binance API Client
        self.client = Client(api_key, secret_key)
        # Binance API websocket
        self.socket = BinanceSocketManager(self.client)
        # Numpy array
        self.data = None
        # Panda dataFrame
        self.dataframe = None
        # Plotly figure
        self.chart = None
        # Dash app
        self.app = dash.Dash()
        # Trading asset
        self.asset = None
        # Chart timeframe
        self.timeframe = None
        # Last update date
        self.last_update = None

    def start(self, asset, timeframe, start_time):
        print("[*] Manager start")
        self.asset = asset
        self.timeframe = timeframe
        self.dataframe = self.get_historical_klines(asset, timeframe, start_time)
        self.chart = self.generate_chart(self.asset, self.timeframe, self.dataframe)
        print("[*] Display chart")
        self.chart.show()
        print("[*] Socket start")
        self.socket.start()
        print("[*] Socket kline start")
        self.socket.start_kline_socket(self.asset, self.klines_callback, interval=self.timeframe)
        return self

    def get_historical_klines(self, asset, timeframe, start_time):
        print("[*] Get historical klines")
        # fetch klines
        fetched_data = self.client.get_historical_klines(asset, timeframe, start_time)
        # Keep only the first 6 columns
        data = np.array(fetched_data)[:, 0:6]
        # Set last update
        self.last_update = data[0, -1]
        # Create a DataFrame with annotated columns
        df = pd.DataFrame(data, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
        # Convert Date from ms to Datetime
        df["Date"] = pd.to_datetime(df["Date"], unit="ms")
        # Convert columns to numeric values
        df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
        df["High"] = pd.to_numeric(df["High"], errors="coerce")
        df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
        df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
        df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
        return df

    def klines_callback(self, msg):
        print("[*] Websocket: Updating last kline")
        if msg['e'] == 'error':
            print("[!] ERROR: Klines websocket does not work.")
            return self

        # [Kline start time, Open price, High price, Low price, Close price, Base asset volume
        kline = np.array([msg['k']['t'], msg['k']['o'], msg['k']['h'], msg['k']['l'], msg['k']['c'], msg['k']['v']])
        # Create a Tableau with annotated columns
        df = pd.DataFrame([kline], columns=["Date", "Open", "High", "Low", "Close", "Volume"])
        # Convert Date from ms to Datetime
        df["Date"] = pd.to_datetime(df["Date"], unit="ms")
        # Convert columns to numeric values
        df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
        df["High"] = pd.to_numeric(df["High"], errors="coerce")
        df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
        df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
        df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
        # Is a new kline?
        if self.last_update != kline[0]:
            # Set last update
            self.last_update = kline[0]
            # Append df to main dataframe
            self.dataframe.append(df)
        else:
            # Update last kline value
            self.dataframe.loc[self.dataframe.tail().index] = df
        # Update chart
        """
        self.chart = go.Figure(layout=go.Layout(title=(self.asset + " " + self.timeframe + " chart"), template="plotly_dark"))
        self.chart.add_trace(go.Candlestick(x=self.dataframe["Date"],
                                            open=self.dataframe["Open"],
                                            high=self.dataframe["High"],
                                            low=self.dataframe["Low"],
                                            close=self.dataframe["Close"],
                                            increasing_line_color='#2ebd85', decreasing_line_color='#e0294a',
                                            increasing_fillcolor='#2ebd85', decreasing_fillcolor='#e0294a',
                                            name="Price"))
        """
        return self

    def generate_chart(self, asset, timeframe, dataframe):
        print("[*] Generate chart")
        # Create Chart
        chart = go.Figure(layout=go.Layout(title=(asset + " " + timeframe + " Chart"), template="plotly_dark"))
        # Candlesticks
        chart.add_trace(go.Candlestick(x=dataframe["Date"],
                                     open=dataframe["Open"],
                                     high=dataframe["High"],
                                     low=dataframe["Low"],
                                     close=dataframe["Close"],
                                     increasing_line_color='#2ebd85', decreasing_line_color='#e0294a',
                                     increasing_fillcolor='#2ebd85', decreasing_fillcolor='#e0294a',
                                     name="Price"))
        # Average Close Price
        chart.add_trace(go.Scatter(x=dataframe["Date"],
                                 y=dataframe["Close"],
                                 line_color="cyan",
                                 name="Close"))
        return chart

    def stop(self):
        print("[*] Manager stop")
        self.socket.close()
        reactor.stop()
        return self


# Data
print("[*] Data")
api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"

# Init
print("[*] Init")
manager = BinanceManager(api_key, secret_key)
manager.start("BTCUSDT", Client.KLINE_INTERVAL_1MINUTE, "1 hour ago UTC")
while 1:
    time.sleep(1)
manager.stop()
