# Imports
import tensorflow as tf

from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt

from binance.client import Client
import mplfinance as mpf
import numpy as np
import pandas as pd


def fetch_data(client, asset, timeframe, start_date, filename=None, read=False):
    """
    Fetch data from Binance API and return a panda DataFrame.
    :param client:
    :param asset:
    :param timeframe:
    :param start_date:
    :param read:
    :return:
    """
    if filename is not None and read:
        try:
            print("[*] Read data")
            df = pd.read_csv(filename)
            # Convert Date from ms to Datetime
            df["Date"] = pd.to_datetime(df["Date"])
            # Set Date column as index
            df.set_index("Date", inplace=True)
            return df
        except:
            print("[!] Error while reading data from given file, fetching new data...")
    # Data
    print("[*] Fetch data")
    # fetch klines
    raw_data = client.get_historical_klines(asset, timeframe, start_date)
    """
    Returned format:
    [
      [
        1499040000000,      // Open time (in miliseconds)
        "0.01634790",       // Open
        "0.80000000",       // High
        "0.01575800",       // Low
        "0.01577100",       // Close
        "148976.11427815",  // Volume
        1499644799999,      // Close time
        "2434.19055334",    // Quote asset volume
        308,                // Number of trades
        "1756.87402397",    // Taker buy base asset volume
        "28.46694368",      // Taker buy quote asset volume
        "17928899.62484339" // Ignore.
      ]
    ]
    """
    # Keep only the first 6 columns
    data = np.array(raw_data)[:, 0:6]
    # Create a Tableau with annotated columns
    df = pd.DataFrame(data, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
    # Convert Date from ms to Datetime
    df["Date"] = pd.to_datetime(df["Date"], unit="ms")
    # Set Date column as index
    df.set_index("Date", inplace=True)
    df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
    df["High"] = pd.to_numeric(df["High"], errors="coerce")
    df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
    df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
    df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
    if filename is not None:
        df.to_csv(filename)
    return df


def plot_ohlc_chart(df, title="Chart", filename=None):
    print("[*] Plot chart")
    # Create Chart Style
    mc = mpf.make_marketcolors(
        up='#2ebd85', down='#e0294a',
        edge={'up': '#2ebd85', 'down': '#e0294a'},
        wick={'up': '#2ebd85', 'down': '#e0294a'},
        inherit=True
    )

    s = mpf.make_mpf_style(base_mpf_style='mike', figcolor="#111111", facecolor="#111111", gridstyle="solid",
                           marketcolors=mc)
    # Display chart
    mpf.plot(df, type="candle", volume=True, style=s, title=title)
    if filename is not None:
        # Save Chart
        mpf.plot(df, type="candle", volume=True, style=s, title="BTC/USDT 5min over 1day", savefig=filename)


if __name__ == "__main__":
    """ Data """
    api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
    secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"
    # Init
    print("[*] Init")
    client = Client(api_key, secret_key)
    # Data
    dataset = fetch_data(client, "BTCUSDT", Client.KLINE_INTERVAL_1HOUR, "1 week ago UTC", "btc_usdt_1h_1w.csv", True)
    # Plot
    # plot_ohlc_chart(dataset)
    """ Preprocessing """

    """ Model """
    model = models.Sequential()
    # OHLC * Hour in Day * Day in Week
    model.add(layers.Dense(4 * 24 * 7, activation='relu'))
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dense(4, activation='relu'))
    """ Training """
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    history = model.fit(train_images, train_labels, epochs=10,
                        validation_data=(test_images, test_labels))
    """ Prediction """
    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label='val_accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.5, 1])
    plt.legend(loc='lower right')

    test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=2)
    print(test_acc)