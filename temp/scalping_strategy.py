"""
Scalping Strategy
"""

# Imports
from binance.client import Client
import numpy as np
import pandas as pd

from ta import add_all_ta_features
from ta.utils import dropna
from ta.volatility import BollingerBands
from ta.trend import IchimokuIndicator
from ta.momentum import RSIIndicator

import plotly.graph_objects as go
from plotly.subplots import make_subplots

import dash
import dash_core_components as dcc
import dash_html_components as html

import mplfinance as mpf

# Data
print("[*] Data")
api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"

# Init
print("[*] Init")
client = Client(api_key, secret_key)

print("[*] Get Market Data")
# fetch 1 Hour klines for the last week
btc_usdt_1m_4h = client.get_historical_klines("BTCUSDT", Client.KLINE_INTERVAL_1MINUTE, "4 hour ago UTC")
# Keep only the first 6 columns
btc_usdt_1m_4h = np.array(btc_usdt_1m_4h)[:, 0:6]
# Create a Tableau with annotated columns
df = pd.DataFrame(btc_usdt_1m_4h, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
# Convert Date from ms to Datetime
df["Date"] = pd.to_datetime(df["Date"], unit="ms")
df.set_index("Date", inplace=True)
# Convert columns to numeric values
df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
df["High"] = pd.to_numeric(df["High"], errors="coerce")
df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
print(df)



# Initialize Bollinger Bands Indicator
indicator_bb = BollingerBands(close=df["Close"], n=20, ndev=2)
# Add Bollinger Bands features
df['bb_bbm'] = indicator_bb.bollinger_mavg()
df['bb_bbh'] = indicator_bb.bollinger_hband()
df['bb_bbl'] = indicator_bb.bollinger_lband()
# Add Bollinger Band high indicator
df['bb_high'] = indicator_bb.bollinger_hband_indicator()
# Add Bollinger Band low indicator
df['bb_low'] = indicator_bb.bollinger_lband_indicator()
# Add Width Size Bollinger Bands
df['bb_width'] = indicator_bb.bollinger_wband()
# Add Percentage Bollinger Bands
df['bb_perc'] = indicator_bb.bollinger_pband()


indicator_ichimoku = IchimokuIndicator(high=df["High"], low=df["Low"], n1=9, n2=26, n3=52)
# Add Senkou Span A
df['ich_senkou_a'] = indicator_ichimoku.ichimoku_a()
# Add Senkou Span B
df['ich_senkou_b'] = indicator_ichimoku.ichimoku_b()
# Add Kijun-sen
df['ich_kijun'] = indicator_ichimoku.ichimoku_base_line()
# Add Tenkan-sen
df['ich_tenkan'] = indicator_ichimoku.ichimoku_conversion_line()
# Add Lagging Span
df['ich_lagging'] = df["Close"].shift(-26)

indicator_rsi = RSIIndicator(close=df["Close"], n=14)
df['rsi'] = indicator_rsi.rsi()

# Create Chart Style
mc = mpf.make_marketcolors(
    up='#2ebd85', down='#e0294a',
    edge={'up': '#2ebd85', 'down': '#e0294a'},
    wick={'up': '#2ebd85', 'down': '#e0294a'},
    inherit=True
)

s = mpf.make_mpf_style(base_mpf_style='mike', figcolor="#111111", facecolor="#111111", gridstyle="solid",
                       marketcolors=mc)
"""
# Multiple data to plot
apds = [ mpf.make_addplot(tcdf),
         mpf.make_addplot(low_signal,scatter=True,markersize=200,marker='^'),
         mpf.make_addplot(high_signal,scatter=True,markersize=200,marker='v'),
       ]

# fill area (kumo): https://matplotlib.org/gallery/lines_bars_and_markers/fill_between_demo.html#sphx-glr-gallery-lines-bars-and-markers-fill-between-demo-py

mpf.plot(df,addplot=apds,figscale=1.25,volume=True)
"""
apds = [
    mpf.make_addplot(df["ich_lagging"], color="green"),
    mpf.make_addplot(df["ich_kijun"], color="red"),
    mpf.make_addplot(df["ich_tenkan"], color="blue"),
    mpf.make_addplot(df["ich_senkou_a"], color="green"),
    mpf.make_addplot(df["ich_senkou_b"], color="red"),
    mpf.make_addplot(df["rsi"], panel=1),
       ]


# Display chart
mpf.plot(df, type="candle", volume=True, style=s, title="BTC/USDT 5min over 1day", addplot=apds, main_panel=0, volume_panel=2,
         fill_between=dict(y1=df["ich_senkou_a"].values, y2=df["ich_senkou_b"].values, where=(df["ich_senkou_a"].values > df["ich_senkou_b"].values), color="#41864f", alpha=0.5))
"""
mpf.plot(df, type="candle", volume=True, style=s, title="BTC/USDT 5min over 1day",
         fill_between=[dict(y1=df["ich_senkou_a"].values, y2=df["ich_senkou_b"].values, where=(df["ich_senkou_a"].values > df["ich_senkou_b"].values), color="#41864f", alpha=0.5),
                           dict(y1=df["ich_senkou_a"].values, y2=df["ich_senkou_b"].values, where=(df["ich_senkou_a"].values < df["ich_senkou_b"].values), color="#FF3333", alpha=0.5)]
         )
"""
# Save Chart
# mpf.plot(df, type="candle", volume=True, style=s, title="BTC/USDT 5min over 1day", savefig="chart_btc_usdt_5m_1d.png")
