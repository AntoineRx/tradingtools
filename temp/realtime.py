# Imports
from binance.client import Client
from binance.websockets import BinanceSocketManager

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

from ta.volatility import BollingerBands
from ta.trend import IchimokuIndicator
from ta.momentum import RSIIndicator

"""
Get data from Binance & Display chart.
"""
import sys
from threading import Thread
import time

DATA_FILENAME = "../temp/data.csv"


"""
Get data from Binance.
"""


class BinanceManager:
    def __init__(self, api_key, secret_key):
        # Binance API Client
        self.client = Client(api_key, secret_key)
        # Binance API websocket
        self.socket = BinanceSocketManager(self.client)
        # Numpy array
        self.data = None
        # Panda dataFrame
        self.dataframe = None
        # Plotly figure
        self.chart = None
        # Trading asset
        self.asset = None
        # Chart timeframe
        self.timeframe = None
        # Last update date
        self.last_update = None

    def start(self, asset, timeframe, start_time):
        print("[*] Manager start")
        self.asset = asset
        self.timeframe = timeframe
        self.dataframe = self.get_historical_klines(asset, timeframe, start_time)
        print("[*] Display chart")
        #self.plotter.start()
        print("[*] Socket start")
        self.socket.start()
        print("[*] Socket kline start")
        self.socket.start_kline_socket(self.asset, self.klines_callback, interval=self.timeframe)
        return self

    def get_historical_klines(self, asset, timeframe, start_time):
        print("[*] Get historical klines")
        # fetch klines
        fetched_data = self.client.get_historical_klines(asset, timeframe, start_time)
        # Keep only the first 6 columns
        data = np.array(fetched_data)[:, 0:6]
        # Set last update
        self.last_update = data[0, -1]
        # Create a DataFrame with annotated columns
        df = pd.DataFrame(data, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
        # Convert Date from ms to Datetime
        df["Date"] = pd.to_datetime(df["Date"], unit="ms")
        # Convert columns to numeric values
        df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
        df["High"] = pd.to_numeric(df["High"], errors="coerce")
        df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
        df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
        df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
        df.to_csv(DATA_FILENAME)
        return df

    def klines_callback(self, msg):
        print("[*] Websocket: Updating last kline")
        if msg['e'] == 'error':
            print("[!] ERROR: Klines websocket does not work.")
            return self

        # [Kline start time, Open price, High price, Low price, Close price, Base asset volume
        kline = np.array([msg['k']['t'], msg['k']['o'], msg['k']['h'], msg['k']['l'], msg['k']['c'], msg['k']['v']])
        # Create a Tableau with annotated columns
        df = pd.DataFrame([kline], columns=["Date", "Open", "High", "Low", "Close", "Volume"])
        # Convert Date from ms to Datetime
        df["Date"] = pd.to_datetime(df["Date"], unit="ms")
        # Convert columns to numeric values
        df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
        df["High"] = pd.to_numeric(df["High"], errors="coerce")
        df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
        df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
        df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
        # Is a new kline?
        if self.dataframe.tail(1).iloc[0]["Date"] == df.tail(1).iloc[0]["Date"]:
            # Update last kline value
            self.dataframe.drop(self.dataframe.tail(1).index, inplace=True)
        # Append df to main dataframe
        self.dataframe = self.dataframe.append(df, ignore_index=True)
        self.apply_indicators()
        # Update chart
        self.dataframe.to_csv(DATA_FILENAME)
        return self
    
    def apply_indicators(self):
        df = self.dataframe
        # Initialize Bollinger Bands Indicator
        indicator_bb = BollingerBands(close=df["Close"], n=20, ndev=2)
        # Add Bollinger Bands features
        df['bb_bbm'] = indicator_bb.bollinger_mavg()
        df['bb_bbh'] = indicator_bb.bollinger_hband()
        df['bb_bbl'] = indicator_bb.bollinger_lband()
        # Add Bollinger Band high indicator
        df['bb_high'] = indicator_bb.bollinger_hband_indicator()
        # Add Bollinger Band low indicator
        df['bb_low'] = indicator_bb.bollinger_lband_indicator()
        # Add Width Size Bollinger Bands
        df['bb_width'] = indicator_bb.bollinger_wband()
        # Add Percentage Bollinger Bands
        df['bb_perc'] = indicator_bb.bollinger_pband()

        indicator_ichimoku = IchimokuIndicator(high=df["High"], low=df["Low"], n1=9, n2=26, n3=52)

        # Add Senkou Span A
        df['ich_senkou_a'] = indicator_ichimoku.ichimoku_a()
        # Add Senkou Span B
        df['ich_senkou_b'] = indicator_ichimoku.ichimoku_b()
        #
        dataframe_extra = pd.DataFrame({'Date': df["Date"].tail(26) + pd.DateOffset(minutes=26)})
        df = df.append(dataframe_extra, ignore_index=True, sort=True)
        df['ich_senkou_a'] = df['ich_senkou_a'].shift(26)
        df['ich_senkou_b'] = df['ich_senkou_b'].shift(26)
        # Red Cloud
        df['kumo_green'] = df['ich_senkou_b'].mask(df['ich_senkou_b'] > df['ich_senkou_a'], df['ich_senkou_a'])
        df['kumo_red'] = df['ich_senkou_a'].mask(df['ich_senkou_a'] > df['ich_senkou_b'], df['ich_senkou_b'])

        # Add Kijun-sen
        df['ich_kijun'] = indicator_ichimoku.ichimoku_base_line()
        # Add Tenkan-sen
        df['ich_tenkan'] = indicator_ichimoku.ichimoku_conversion_line()
        # Add Lagging Span
        df['ich_lagging'] = df["Close"].shift(-26)

        indicator_rsi = RSIIndicator(close=df["Close"], n=14)
        df['rsi'] = indicator_rsi.rsi()
        df.to_csv("data_updated.csv")

    def stop(self):
        print("[*] Manager stop")
        self.socket.close()
        return self


if __name__ == "__main__":
    # Data
    print("[*] Data")
    api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
    secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"

    # Init
    print("[*] Init")
    manager = BinanceManager(api_key, secret_key)
    manager.start("BTCUSDT", Client.KLINE_INTERVAL_1MINUTE, "4 hour ago UTC")
    try:
        while True:
            time.sleep(1)
    # Ctrl + C
    except KeyboardInterrupt:
        manager.stop()
