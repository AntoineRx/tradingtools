"""
Decision Tree Model -> Determine next candle move based on single candle & TA
Output => +-100%

Open
High
Low
Close
BB High
BB Low
ICHMK SENKOU A
ICHMK SENKOU B
ICHMK KIJUN
ICHMK TENKAN
ICHMK CHIKOU
RSI
"""

# Imports
from binance.client import Client
import numpy as np
import pandas as pd
from ta.volatility import BollingerBands
from ta.trend import IchimokuIndicator
from ta.momentum import RSIIndicator


# Global variables
FILENAME_DATA_RAW = "data.csv"
FILENAME_DATA_TA = "data_ta.csv"
FILENAME_DATA_FORECAST = "data_forecast.csv"
API_KEY = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
SECRET_KEY = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"


# Fetch data
def fetch(api_key, secret_key, asset, timeframe, start_time, filename=None, force_update=False):
    # Read data from given file
    if filename is not None and not force_update:
        return pd.read_csv(filename)
    # Binance API Client
    client = Client(api_key, secret_key)
    print("[*] Get historical klines")
    # fetch klines
    fetched_data = client.get_historical_klines(asset, timeframe, start_time)
    # Keep only the first 6 columns
    data = np.array(fetched_data)[:, 0:6]
    # Create a DataFrame with annotated columns
    df = pd.DataFrame(data, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
    # Convert Date from ms to Datetime
    df["Date"] = pd.to_datetime(df["Date"], unit="ms")
    # Convert columns to numeric values
    df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
    df["High"] = pd.to_numeric(df["High"], errors="coerce")
    df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
    df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
    df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
    # Write data to given file
    if filename is not None:
        df.to_csv(filename)
    return df


# Prediction Model
class Model:
    def __init__(self):
    def preprocessing(self):
        # Normalize Each Rows
    def fit(self):
    def predict(self):
    def forecast(self, steps):


    def apply_indicators(self):
        df = self.df
        # Initialize Bollinger Bands Indicator
        indicator_bb = BollingerBands(close=df["Close"], n=20, ndev=2)
        # Add Bollinger Bands features
        df['bb_bbm'] = indicator_bb.bollinger_mavg()
        df['bb_bbh'] = indicator_bb.bollinger_hband()
        df['bb_bbl'] = indicator_bb.bollinger_lband()
        # Add Bollinger Band high indicator
        df['bb_high'] = indicator_bb.bollinger_hband_indicator()
        # Add Bollinger Band low indicator
        df['bb_low'] = indicator_bb.bollinger_lband_indicator()
        # Add Width Size Bollinger Bands
        df['bb_width'] = indicator_bb.bollinger_wband()
        # Add Percentage Bollinger Bands
        df['bb_perc'] = indicator_bb.bollinger_pband()

        indicator_ichimoku = IchimokuIndicator(high=df["High"], low=df["Low"], n1=9, n2=26, n3=52)

        # Add Senkou Span A
        df['ich_senkou_a'] = indicator_ichimoku.ichimoku_a()
        # Add Senkou Span B
        df['ich_senkou_b'] = indicator_ichimoku.ichimoku_b()
        #
        df_extra = pd.DataFrame({'Date': df["Date"].tail(26) + pd.DateOffset(minutes=26)})
        df = df.append(df_extra, ignore_index=True, sort=True)
        df['ich_senkou_a'] = df['ich_senkou_a'].shift(26)
        df['ich_senkou_b'] = df['ich_senkou_b'].shift(26)
        # Add Kijun-sen
        df['ich_kijun'] = indicator_ichimoku.ichimoku_base_line()
        # Add Tenkan-sen
        df['ich_tenkan'] = indicator_ichimoku.ichimoku_conversion_line()
        # Add Lagging Span
        df['ich_lagging'] = df["Close"].shift(-26)

        indicator_rsi = RSIIndicator(close=df["Close"], n=14)
        df['rsi'] = indicator_rsi.rsi()
        df.to_csv("data_updated.csv")



if __name__ == "__main__":
    # Init
    print("[*] Init")
    # Data
    print("[*] Data")

    df = fetch("BTCUSDT", Client.KLINE_INTERVAL_1MINUTE, "4 hour ago UTC")
