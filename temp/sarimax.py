# Imports
from scipy.stats import norm
import statsmodels.api as sm
import matplotlib.pyplot as plt

from binance.client import Client
import mplfinance as mpf
import numpy as np
import pandas as pd


def fetch_data(client, asset, timeframe, start_date, filename=None, read=False):
    """
    Fetch data from Binance API and return a panda DataFrame.
    :param client:
    :param asset:
    :param timeframe:
    :param start_date:
    :param read:
    :return:
    """
    if filename is not None and read:
        try:
            print("[*] Read data")
            df = pd.read_csv(filename)
            # Convert Date from ms to Datetime
            df["Date"] = pd.to_datetime(df["Date"])
            # Set Date column as index
            df.set_index("Date", inplace=True)
            return df
        except:
            print("[!] Error while reading data from given file, fetching new data...")
    # Data
    print("[*] Fetch data")
    # fetch klines
    raw_data = client.get_historical_klines(asset, timeframe, start_date)
    """
    Returned format:
    [
      [
        1499040000000,      // Open time (in miliseconds)
        "0.01634790",       // Open
        "0.80000000",       // High
        "0.01575800",       // Low
        "0.01577100",       // Close
        "148976.11427815",  // Volume
        1499644799999,      // Close time
        "2434.19055334",    // Quote asset volume
        308,                // Number of trades
        "1756.87402397",    // Taker buy base asset volume
        "28.46694368",      // Taker buy quote asset volume
        "17928899.62484339" // Ignore.
      ]
    ]
    """
    # Keep only the first 6 columns
    data = np.array(raw_data)[:, 0:6]
    # Create a Tableau with annotated columns
    df = pd.DataFrame(data, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
    # Convert Date from ms to Datetime
    df["Date"] = pd.to_datetime(df["Date"], unit="ms")
    # Set Date column as index
    df.set_index("Date", inplace=True)
    df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
    df["High"] = pd.to_numeric(df["High"], errors="coerce")
    df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
    df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
    df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
    if filename is not None:
        df.to_csv(filename)
    return df


def plot_ohlc_chart(df, title="Chart", filename=None):
    print("[*] Plot chart")
    # Create Chart Style
    mc = mpf.make_marketcolors(
        up='#2ebd85', down='#e0294a',
        edge={'up': '#2ebd85', 'down': '#e0294a'},
        wick={'up': '#2ebd85', 'down': '#e0294a'},
        inherit=True
    )

    s = mpf.make_mpf_style(base_mpf_style='mike', figcolor="#111111", facecolor="#111111", gridstyle="solid",
                           marketcolors=mc)
    # Display chart
    mpf.plot(df, type="candle", volume=True, style=s, title=title)
    if filename is not None:
        # Save Chart
        mpf.plot(df, type="candle", volume=True, style=s, title="BTC/USDT 5min over 1day", savefig=filename)


if __name__ == "__main__":
    api_key = "5EgE2chLQ34qrmqSNtdGR1q1m3UkdjYZzwIMq0vIj2SsHIPflsldjhYG27HH9GCl"
    secret_key = "pbhAODkGWbLJDhwl9YCvZkipIru1JhNV2LFejqxMOWD6IX28HeRA7Pw0TEcqHQKZ"
    # Init
    print("[*] Init")
    client = Client(api_key, secret_key)
    # Data
    dataset = fetch_data(client, "BTCUSDT", Client.KLINE_INTERVAL_1HOUR, "1 week ago UTC", "btc_usdt_1h_1w.csv", True)
    # Plot
    # plot_ohlc_chart(dataset)
    # Mean
    dataset["Mean"] = (dataset["Low"] + dataset["High"]) / 2
    # Dataset for prediction
    dataset_for_prediction = dataset.copy()
    dataset_for_prediction["Actual"] = dataset_for_prediction["Mean"].shift(-1)
    dataset_for_prediction = dataset_for_prediction.dropna()
    # Plot
    print("[*] Plot")
    dataset_for_prediction["Mean"].plot(color="green", figsize=(15, 2))
    plt.legend(["Next day value", "Mean"])
    plt.title("BTC/USDT 1h over 1w")
    plt.show()
    #
    from sklearn.preprocessing import MinMaxScaler

    sc_in = MinMaxScaler(feature_range=(0, 1))
    scaled_input = sc_in.fit_transform(dataset_for_prediction[["Low", "High", "Open", "Close", "Volume", "Mean"]])
    scaled_input = pd.DataFrame(scaled_input)
    X = scaled_input
    sc_out = MinMaxScaler(feature_range=(0, 1))
    scaler_output = sc_out.fit_transform(dataset_for_prediction[["Actual"]])
    scaler_output = pd.DataFrame(scaler_output)
    y = scaler_output
    X.rename(columns={0: "Low", 1: "High", 2: "Open", 3: "Close", 4: "Volume", 5: "Mean"}, inplace=True)
    y.rename(columns={0: "Stock Price next day"}, inplace=True)
    y.index = dataset_for_prediction.index
    train_size = int(len(dataset) * 0.7)
    test_size = int(len(dataset)) - train_size
    train_X, train_y = X[:train_size].dropna(), y[:train_size].dropna()
    test_X, test_y = X[train_size:].dropna(), y[train_size:].dropna()

    import statsmodels.api as sm

    seas_d = sm.tsa.seasonal_decompose(X["Mean"], model="add", period=24)
    fig = seas_d.plot()
    fig.set_figheight(4)
    plt.show()

    from statsmodels.tsa.stattools import adfuller
    def test_adf(series, title=''):
        dfout = {}
        dftest = sm.tsa.adfuller(series.dropna(), autolag='AIC', regression='ct')
        for key, val in dftest[4].items():
            dfout[f'critical value ({key})'] = val
        if dftest[1] <= 0.05:
            print("Strong evidence against Null Hypothesis")
            print("Reject Null Hypothesis - Data is Stationary")
            print("Data is Stationary", title)
        else:
            print("Strong evidence for  Null Hypothesis")
            print("Accept Null Hypothesis - Data is not Stationary")
            print("Data is NOT Stationary for", title)


    y_test = y["Stock Price next day"][:train_size].dropna()
    test_adf(y_test, " Stock Price")
    test_adf(y_test.diff(), "Stock Price")
    
    fig, ax = plt.subplots(2, 1, figsize=(10, 5))
    fig = sm.tsa.graphics.plot_acf(y_test, lags=50, ax=ax[0])
    fig = sm.tsa.graphics.plot_pacf(y_test, lags=50, ax=ax[1])
    plt.show()

    from pmdarima.arima import auto_arima
    step_wise=auto_arima(train_y, 
     exogenous= train_X,
     start_p=1, start_q=1, 
     max_p=7, max_q=7, 
     d=1, max_d=7,
     trace=True, 
     error_action="ignore", 
     suppress_warnings=True, 
     stepwise=True)

    step_wise.summary()
    from statsmodels.tsa.statespace.sarimax import SARIMAX

    model = SARIMAX(train_y,
                    exog=train_X,
                    order=(0, 1, 1),
                    enforce_invertibility=False, enforce_stationarity=False)
    results = model.fit()
    predictions = results.predict(start=train_size, end=train_size + test_size + (-1) - 1, exog=test_X)
    forecast_1 = results.forecast(steps=test_size - 1, exog=test_X)
    act = pd.DataFrame(scaler_output.iloc[train_size:, 0])

    predictions = pd.DataFrame(predictions)
    predictions.reset_index(drop=True, inplace=True)
    predictions.index = test_X.index
    predictions['Actual'] = act['Stock Price next day']
    predictions.rename(columns={0: 'Pred'}, inplace=True)

    predictions["Actual"].plot(figsize=(20, 8), legend=True, color="blue")
    predictions["Pred"].plot(legend=True, color="red", figsize=(20, 8))

    forecast_apple = pd.DataFrame(forecast_1)
    forecast_apple.reset_index(drop=True, inplace=True)
    forecast_apple.index = test_X.index
    forecast_apple["Actual"] = scaler_output.iloc[train_size:, 0]
    forecast_apple.rename(columns={0:"Forecast"}, inplace=True)

    forecast_apple["Forecast"].plot(legend=True)
    forecast_apple["Actual"].plot(legend=True)

    from statsmodels.tools.eval_measures import rmse

    error = rmse(predictions["Pred"], predictions["Actual"])

    trainPredict = sc_out.inverse_transform(predictions[["Pred"]])
    testPredict = sc_out.inverse_transform(predictions[["Actual"]])