# Imports
import os
from dotenv import load_dotenv


import sys
from threading import Thread
import time


from binance.client import Client
from binance.websockets import BinanceSocketManager

import numpy as np
import pandas as pd


"""
Get real-time data from Binance.
"""

BINANCE_API_KEY = os.getenv('BINANCE_API_KEY')
BINANCE_API_SECRET = os.getenv('BINANCE_API_SECRET')
FILE_DATA = os.getenv('FILE_DATA')


class BinanceManager:
    def __init__(self, api_key, secret_key):
        # Binance API Client
        self.client = Client(api_key, secret_key)
        # Binance API websocket
        self.socket = BinanceSocketManager(self.client)
        # Panda dataFrame
        self.df = None
        # Trading asset
        self.asset = None
        # Chart timeframe
        self.timeframe = None
        # Logger
        self.logger = Logger()

    def start(self, asset, timeframe, start_time):
        print("[*] Manager start")
        self.asset = asset
        self.timeframe = timeframe
        self.df = self.get_historical_klines(asset, timeframe, start_time)
        #self.plotter.start()
        self.logger.success("Socket start")
        self.socket.start()
        self.logger.success("Socket kline start")
        self.socket.start_kline_socket(self.asset, self.klines_callback, interval=self.timeframe)
        return self

    def get_historical_klines(self, asset, timeframe, start_time):
        self.logger.success("Get historical klines")
        # fetch klines
        data = self.client.get_historical_klines(asset, timeframe, start_time)
        # Keep only the first 6 columns
        data = np.array(data)[:, 0:6]
        # Create a DataFrame with annotated columns
        df = pd.DataFrame(data, columns=["Date", "Open", "High", "Low", "Close", "Volume"])
        # Convert Date from ms to Datetime
        df["Date"] = pd.to_datetime(df["Date"], unit="ms")
        # Convert columns to numeric values
        df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
        df["High"] = pd.to_numeric(df["High"], errors="coerce")
        df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
        df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
        df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
        df.to_csv(FILE_DATA)
        return df

    def klines_callback(self, msg):
        self.logger.success("Websocket: Updating last kline")
        if msg['e'] == 'error':
            print("[!] ERROR: Klines websocket does not work.")
            return self

        # [Kline start time, Open price, High price, Low price, Close price, Base asset volume
        kline = np.array([msg['k']['t'], msg['k']['o'], msg['k']['h'], msg['k']['l'], msg['k']['c'], msg['k']['v']])
        # Create a Tableau with annotated columns
        df = pd.DataFrame([kline], columns=["Date", "Open", "High", "Low", "Close", "Volume"])
        # Convert Date from ms to Datetime
        df["Date"] = pd.to_datetime(df["Date"], unit="ms")
        # Convert columns to numeric values
        df["Open"] = pd.to_numeric(df["Open"], errors="coerce")
        df["High"] = pd.to_numeric(df["High"], errors="coerce")
        df["Low"] = pd.to_numeric(df["Low"], errors="coerce")
        df["Close"] = pd.to_numeric(df["Close"], errors="coerce")
        df["Volume"] = pd.to_numeric(df["Volume"], errors="coerce")
        # If is not a new kline
        if self.df.tail(1).iloc[0]["Date"] == df.tail(1).iloc[0]["Date"]:
            # Update last kline value
            self.df.drop(self.df.tail(1).index, inplace=True)
        # Append df to main df
        self.df = self.df.append(df, ignore_index=True)
        # Update chart
        self.df.to_csv(FILE_DATA)
        return self

    def stop(self):
        print("[*] Manager stop")
        self.socket.close()
        return self


if __name__ == "__main__":
    # Init
    print("[*] Init")
    manager = BinanceManager(BINANCE_API_KEY, BINANCE_API_SECRET)
    manager.start("BTCUSDT", Client.KLINE_INTERVAL_1MINUTE, "4 hour ago UTC")
    try:
        while True:
            time.sleep(1)
    # Ctrl + C
    except KeyboardInterrupt:
        manager.stop()
