import datetime
import pandas as pd
import plotly.graph_objects as go
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly
from dash.dependencies import Input, Output

# https://community.plotly.com/t/y-axis-autoscaling-with-x-range-sliders/10245/3

app = dash.Dash(__name__)
app.layout = html.Div(
    html.Div([
        dcc.Graph(id='live-update-graph', style={ "height" : "98vh"}),
        dcc.Interval(
            id='interval-component',
            interval=2000,
            n_intervals=10
        )
    ], style={ "height" : "100%"}) , style={ "height" : "100%"}
)


# Multiple components can update everytime interval gets fired.
@app.callback(Output('live-update-graph', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_graph_live(n):
    df = pd.read_csv('../temp/data_updated.csv')
    # Create Chart
    fig = go.Figure(layout=go.Layout(title="BTC/USDT 1Hour over 1Week Chart", template="plotly_dark"))

    # Candlesticks
    fig.add_trace(go.Candlestick(x=df["Date"],
                                 open=df["Open"],
                                 high=df["High"],
                                 low=df["Low"],
                                 close=df["Close"],
                                 increasing_line_color='#2ebd85', decreasing_line_color='#e0294a',
                                 increasing_fillcolor='#2ebd85', decreasing_fillcolor='#e0294a',
                                 name="Price",
                                 yaxis="y2"))


    blue = "rgba(69, 203, 227, 0.42)"
    # Bollinger High
    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["bb_bbh"],
                             line_color=blue,
                             name="BB High",
                             fill=None))

    # Bollinger Low
    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["bb_bbl"],
                             line_color=blue,
                             name="BB Low",
                             fill="tonexty",
                             fillcolor=blue))

    red = "rgba(206, 75, 77, 0.62)"
    green = "rgba(46, 168, 68, 0.62)"
    # Senkou Span A
    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["ich_senkou_a"],
                             line_color=green,
                             name="Senkou Span A",
                             fill=None,
                             connectgaps=True))

    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["kumo_green"],
                             line_color=red,
                             name="kumo green",
                             fill="tonexty",
                             fillcolor=green,
                             connectgaps=False,
                             showlegend=False))

    # Senkou Span B
    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["ich_senkou_b"],
                             line_color=red,
                             name="Senkou Span B",
                             fill=None,
                             connectgaps=True))

    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["kumo_red"],
                             line_color=green,
                             name="kumo red",
                             fill="tonexty",
                             fillcolor=red,
                             connectgaps=False,
                             showlegend=False))

    # Tenkan
    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["ich_tenkan"],
                             line_color="cyan",
                             name="Tenkan",
                             fill=None,
                             connectgaps=True))

    purple = "rgba(230, 86, 225, 1)"
    # Kijun
    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["ich_kijun"],
                             line_color=purple,
                             name="Kijun",
                             fill=None,
                             connectgaps=True))

    # Lagging
    fig.add_trace(go.Scatter(x=df["Date"],
                             y=df["ich_lagging"],
                             line_color="rgba(44, 237, 73, 1)",
                             name="Lagging",
                             fill=None))

    padding = 0.0025
    range = [df["Low"].min() * (1 - padding), df["High"].max() * (1 + padding)]

    fig.update_layout(xaxis_rangeslider_visible=False,
                        yaxis=dict(range=range),
                      yaxis2=dict(overlaying='y', range=range), uirevision='False')
    return fig


if __name__ == '__main__':
    app.run_server(debug=True)
