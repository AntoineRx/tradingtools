"""
Analyse data in real-time and report signals
"""

import os
import random

import discord
from dotenv import load_dotenv

import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import FileSystemEventHandler


from ta.volatility import BollingerBands
from ta.trend import IchimokuIndicator
from ta.momentum import RSIIndicator



PATH_DATA = os.getenv('PATH_DATA')
FILE_DATA = os.getenv('FILE_DATA')
FILE_SIGNALS = os.getenv('FILE_SIGNALS')


class Strategy:
    def analyse(self, df):
        return pd.Dataframe()


class StrategyStochRSI:



class StrategyIchimoku(Strategy):
    def __init__(self):
        self.df_analysis = None

    def apply(self, df, **args):
        self.df_analysis = df
        indicator_ichimoku = IchimokuIndicator(high=df["High"], low=df["Low"], n1=args[0], n2=args[1], n3=args[2])
        # Add Senkou Span A
        self.df_analysis['SenkouA'] = indicator_ichimoku.ichimoku_a()
        # Add Senkou Span B
        self.df_analysis['SenkouB'] = indicator_ichimoku.ichimoku_b()
        # Add offset to SenkouA and SenkouB
        dataframe_extra = pd.DataFrame({'Date': self.df_analysis["Date"].tail(args[1]) + pd.DateOffset(minutes=args[1])})
        self.df_analysis = self.df_analysis.append(dataframe_extra, ignore_index=True, sort=True)
        self.df_analysis['SenkouA'] = self.df_analysis['SenkouA'].shift(args[1])
        self.df_analysis['SenkouB'] = self.df_analysis['SenkouB'].shift(args[1])
        """
        # Kumo
        self.df_analysis['KumoGreen'] = self.df_analysis['SenkouB'].mask(self.df_analysis['SenkouB'] > self.df_analysis['SenkouA'], self.df_analysis['SenkouA'])
        self.df_analysis['KumoRed'] = self.df_analysis['SenkouA'].mask(self.df_analysis['SenkouA'] > self.df_analysis['SenkouB'], self.df_analysis['SenkouB'])
        """
        # Add Kijun-sen
        self.df_analysis['Kijun'] = indicator_ichimoku.ichimoku_base_line()
        # Add Tenkan-sen
        self.df_analysis['Tenkan'] = indicator_ichimoku.ichimoku_conversion_line()
        # Add Lagging Span
        self.df_analysis['Chikou'] = self.df_analysis["Close"].shift(-args[1])
    
    def tenkan_kijun_cross():
    def kijun_cross():
    def kumo_breakout():
    def senkou_cross():
    def chikou_cross():

    def analyse(self, df):
        self.apply(df, 9, 26, 52)
        # Strategy, Score, Message, Chart
        report = pd.Dataframe()
        report = self.tenkan_kijun_cross(df, report)
        report = self.kijun_cross(df, report)
        report = self.kumo_breakout(df, report)
        report = self.senkou_cross(df, report)
        report = self.chikou_cross(df, report)
        return report

class SignalAnalyser:
    def __init__(self):
        self.strategies = []
        self.report = pd.Dataframe()
    
    def add_strategy(self, strategy):
        self.strategies.append(strategy)

    def analyse(self, df):
        for strategy in self.strategies:
            self.report.append(strategy.analyse(df))
        self.report.to_csv(FILE_SIGNALS)
        return self.report


class SignalAnalyserHandler(FileSystemEventHandler):
    def __init__(self, signal_analyser):
        self.signal_analyser = 
    
    def on_modified(self, event):
        if event.src_path == os.path.join(PATH_DATA, FILE_DATA):
            print("Signal")
        print("Updated file:", event.src_path)


if __name__ == "__main__":
    observer = Observer()
    observer.schedule(SignalAnalyserHandler(), path=PATH_DATA, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    # Ctrl + C
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
