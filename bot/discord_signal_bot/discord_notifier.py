"""
Watch for file content update and notify users on Discord Channel

DISCORD: https://realpython.com/how-to-make-a-discord-bot-python/
"""

import os
import random

import discord
from dotenv import load_dotenv

import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import FileSystemEventHandler


DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')
PATH_DATA = os.getenv('PATH_DATA')
FILE_SIGNALS = os.getenv('FILE_SIGNALS')


class DiscordNotifier():
    def __init__(self):
        self.client = discord.Client()
        self.register_events()
        self.start_client()

    def register_events(self):
        @self.client.event
        async def on_message(message):
            if message.author == self.client.user:
                return
            if message.content == '!update':
                await message.channel.send('Update')

    def start_client(self):
        self.client.run(DISCORD_TOKEN)


class DiscordNotifierHandler(FileSystemEventHandler):
    def __init__(self):
        self.discord_notifier = DiscordNotifier()

    def on_modified(self, event):
        if event.src_path == os.path.join(PATH_DATA, FILE_SIGNALS):
            print("Signal")
            df = pd.read_csv(FILE_SIGNALS)
            message = ""
            for row, index in df.iterrows:
                message = ""
                for col, index_col in df.itercols:
                    message += col.name + ": " + col.value + "\n"
                await self.client.schedule.send(message)
        print("Updated file:", event.src_path)


if __name__ == "__main__":
    observer = Observer()
    observer.schedule(DiscordNotifierHandler(), path=FILE_SIGNALS, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    # Ctrl + C
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
